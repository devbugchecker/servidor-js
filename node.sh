#!/bin/bash
# Exemplo Final de Script Shell
#


Principal() {
  echo "Deploy de aplicação Node js com SSL e Reverse-Proxy em VPS Linux"
  echo "------------------------------------------"
  echo "Opções:"
  echo
  echo "1. Instalar Node.js + PM2 +  Let’s Encrypt + Nginx Reverse-Proxy "
  echo "2. Renovação automática do certificado"
  echo "10. Sair do exemplo"
  echo
  echo -n "Qual a opção desejada? "
  read opcao
  case $opcao in
    1) Instalar ;;
    2) Renovar ;;
    10) exit ;;
    *) "Opção desconhecida." ; echo ; Principal ;;
  esac
}

Instalar() {
  echo "Atualizando Sistema...."
  sudo apt-get update
  sleep 3
  clear
  echo "Instalado Node.js...."
  cd ~
  sleep 2
  sudo apt-get install nodejs
  echo "Instalado Build-essential...."
  sudo apt-get install build-essential
  clear
  echo "Criando aplicação Node.js"
  cd ~
  clear
  echo "Instalando PM2"
  sudo npm install -g pm2
  clear
  echo "Vamos configurar o PM2 para iniciar junto ao servidor"  
  sudo pm2 startup systemd
  sudo pm2 save
  clear
  echo "Instalando Let’s Encrypt e gerando certificado SSL"
  sleep 3
  sudo apt-get install letsencrypt -y
  sudo letsencrypt certonly --standalone
  clear


  echo "############# Node.js + PM2 +  Let’s Encrypt Instalando com sucesso :) #############"
  sleep 3
  echo "############# Instalando Nginx e configurando Reverse-Proxy com SSL    #############"
  sudo apt-get install nginx -y
  echo -n "Digite o  VirtualHost  [/var/www/html/] : "
  read root
  echo -n "Enter the VirtualHost name : "
  read name
  mkdir "/var/www/html/"$root 
  cd root
  sleep 1
  echo 'var http = require("http");' > index.js
  echo 'http.createServer(function (req, res) {'>> index.js
  echo '  res.writeHead(200, {"Content-Type": "text/plain"});'>> index.js
  echo '  res.end("Hello World\n");'>> index.js
  echo '}).listen(8080);'>> index.js
  echo 'console.log("Servidor rodando na porta 8080");'>> index.js

  sudo echo "server {
        listen 80;
        listen [::]:80 default_server ipv6only=on;
        return 301 https://$host$request_uri;
        }
        server {
        listen 443;
        server_name '"$name"';
        ssl on;
        
        ssl_certificate /etc/letsencrypt/live/'"$root"'/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/'"$root"'/privkey.pem;
        ssl_session_timeout 5m;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
        # Pass requests for / to localhost:8080:
        location / {
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-NginX-Proxy true;
                proxy_pass http://localhost:8080/;
                proxy_ssl_session_reuse off;
                proxy_set_header Host $http_host;
                proxy_cache_bypass $http_upgrade;
                proxy_redirect off;
        }
}" > /etc/nginx/sites-available/default
  sudo nginx -t
  sudo systemctl start nginx
  node index.js
  pm2 start index.js
}

Renovar() {
  clear
  sudo crontab -e
  30 2 * * 1 /usr/bin/letsencrypt renew >> /var/log/le-renew.log
  35 2 * * 1 /bin/systemctl reload nginx
}

Principal